package com.xieke.java8.demo.abstractfactory;

/**
 * 发送接口
 * 
 * @author 邪客
 *
 */
public interface Sender {  

	/**
	 * 发送方法
	 */
	void Send();

}  